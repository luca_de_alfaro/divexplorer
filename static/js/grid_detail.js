let sleep = (ms) => {
    return function (x) {
        return new Promise(resolve => setTimeout(() => resolve(x), ms));
    };
}

Vue.component('v-select', VueSelect.VueSelect);
let app = new Vue({
    el: "#vue",
    data: {
        get_url: get_url,
        set_class_url: set_class_url,
        compute_url: compute_url,
        dataframe_url: dataframe_url,
        dataframe_view_url: dataframe_view_url,
        delete_dataframe_url: delete_url,
        settings: {
            true_class: '',
            pred_class: '',
            min_supp: 0.01,
            pos_class: "1",
            neg_class: "0",
            rows: [],
            selected: []
        },
        loading: true,
        dataframes: [],
        showGrid: false,
        error: {
            status: 300,
            message: "",
            active: false
        },
    },
    methods: {
        mapCols: function (cols) {
            let self = this;
            return cols.map(function (i) { return self.settings.rows[i]; });
        },
        isDisabled: function () {
            let self = this;
            if (self.settings.true_class === '' ||
                self.settings.min_supp < 0.01 ||
                self.settings.min_supp > 1 ||
                self.settings.neg_class === "" ||
                self.settings.pos_class === "") {
                return true;
            }
            return false;
        },
        redirect: function (id) {
            let self = this;
            window.location.href = self.dataframe_view_url + '/' + id;
        },
        gridDelete: function (id) {
            let self = this;
            self.loading = true;
            axios.post(self.delete_dataframe_url, {
                id: id
            }).then(function (res) {
                axios.get(dataframe_url).then((result) => {
                    self.dataframes = result.data.dataframes;
                    self.loading = false;
                });
            }).catch(function (res) {
                self.loading = false;
                console.log(res);
            });
        },
        computeDataset: function () {
            let self = this;
            if (self.settings.true_class === '') return;

            if (!(self.settings.selected.includes(self.settings.true_class)
                && self.settings.selected.includes(self.settings.pred_class))) {
                self.error.status = 400;
                self.error.message = "Both the True and Predicted Class MUST be in the selected columns.";
                self.error.active = true;
                sleep(3000)()
                    .then(() => {
                        self.error.active = false;
                    });
                return;
            }

            self.loading = true;
            axios.post(self.set_class_url, {
                true_class: self.settings.true_class,
                pred_class: self.settings.pred_class,
                min_supp: self.settings.min_supp,
                neg_class: self.settings.neg_class,
                pos_class: self.settings.pos_class,
                selected_cols: self.settings.selected
            }).then(function (res) {
                axios.post(self.compute_url)
                    .then(function (res) {
                        axios.get(dataframe_url).then((result) => {
                            self.dataframes = result.data.dataframes;
                            self.loading = false;
                        });
                    }).catch(function (res) {
                        let resp = res.response;
                        if (resp.status === 400) {
                            self.error.status = 400;
                            self.error.message = "There was an error computing your divergence. Please try again.";
                            self.error.active = true;
                        }
                        sleep(3000)()
                            .then(() => {
                                self.error.active = false;
                                self.loading = false;
                            });
                    });
            }).catch(function (res) {
                let resp = res.response;
                self.loading = false;
                if (resp.status === 400) {
                    self.error.status = 400;
                    self.error.message = "Incorrect Parameters Set. Please try again.";
                    self.error.active = true;
                }
                sleep(3000)()
                    .then(() => {
                        self.error.active = false;
                    });
            });
        }
    },
    created: function () {
        let self = this;
        self.loading = true;
        axios.get(get_url).then((result) => {
            let data = result.data.data;
            self.settings.rows = result.data.columns;
            self.settings.selected = result.data.columns;
            self.settings.min_supp = data['min_supp'];
            self.settings.true_class = data["true_class"];
            self.settings.pred_class = data["pred_class"];
            self.settings.pos_class = data["pos_class"];
            self.settings.neg_class = data['neg_class'];
            axios.get(dataframe_url).then((result) => {
                self.dataframes = result.data.dataframes;
                self.loading = false;
            });
        }).catch(function (res) {
            let resp = res.response;
            if (resp.status === 410) {
                self.error.status = 410;
                self.error.message = "There was an error loading the dataset. Please delete this dataset and recreate it.";
                self.error.active = true;
            }
            if (resp.status === 400) {
                self.error.status = 400;
                self.error.message = `Bad argument, unable to compute Global Shapley with ${field}.`;
                self.error.active = true;
                sleep(3000)()
                    .then(() => {
                        self.error.active = false;
                    });
            }
        });
    }
});