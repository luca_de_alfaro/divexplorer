Vue.component('vue-plotly', window['vue-plotly'].Plotly);
let sleep = (ms) => {
    return function (x) {
        return new Promise(resolve => setTimeout(() => resolve(x), ms));
    };
}

let app = new Vue({
    el: "#vue",
    data: {
        computeGlobal_url: computeGlobal_url,
        displayShapely_url: displayShapely_url,
        page: '',
        loading: true,
        error: {
            status: 300,
            message: "",
            active: false
        },
        image1: "",
        image2: "",
        itemized: "",
        plotly_fields: {
            data: [],
            layout: {}
        },
        ItemsetName: "",
        field_name: field_name,
        min_thresh: min_thresh,
        corrective: corrective === "True",
    },
    methods: {
        goto: function (page) {
            this.page = page;
        },
        redirect: function (id) {

            window.location.href = "" + '/' + id;
        },
        doAnalyze: function (row, field) {
            let self = this;
            self.field = field;
            axios.post(self.displayShapely_url,
                {
                    field: field,
                    row: row,
                })
                .then(function (res) {
                    self.itemized = "";
                    self.itemized = res.data.imgbytes1;
                    self.plotly_fields["data"] = [];
                    self.plotly_fields["data"] = res.data.figdata["data"];
                    self.plotly_fields["layout"] = {};
                    self.plotly_fields["layout"] = res.data.figdata["layout"];
                    self.ItemsetName = res.data.name;
                    self.goto("individual");
                }).catch(function (res) {
                    let resp = res.response;
                    if (resp.status === 410) {
                        self.error.status = 410;
                        self.error.message = "There was an error loading the dataset. Please delete this dataset and recompute the divergence.";
                        self.error.active = true;
                    }
                    if (resp.status === 400) {
                        self.error.status = 400;
                        self.error.message = `Bad argument, unable to compute Individual Shapley with ${field} on Row: ${row}.`;
                        self.error.active = true;
                        sleep(3000)()
                            .then(() => {
                                self.error.active = false;
                            });
                    }
                });
        },
        computeGlobal: function (field) {
            let self = this;
            axios.post(self.computeGlobal_url,
                {
                    field: field
                })
                .then(function (res) {
                    self.loading = false;
                    self.image1 = "";
                    self.image1 = res.data.imgbytes1;
                    self.image2 = "";
                    self.image2 = res.data.imgbytes2;
                    self.goto("image");
                }).catch(function (res) {
                    let resp = res.response;
                    if (resp.status === 410) {
                        self.error.status = 410;
                        self.error.message = "There was an error loading the dataset. Please delete this dataset and recompute the divergence.";
                        self.error.active = true;
                        sleep(3000)()
                            .then(() => {
                                self.error.active = false;
                            });
                    }
                    if (resp.status === 400) {
                        self.error.status = 400;
                        self.error.message = `Bad argument, unable to compute Global Shapley with ${field}.`;
                        self.error.active = true;
                        sleep(3000)()
                            .then(() => {
                                self.error.active = false;
                            });
                    }
                });
        },
        setThreshold: function (field_name, min_thresh) {
            let self = this;
            self.field_name = field_name;
            self.min_thresh = min_thresh;
        },
        setComp(corrective) {
            let self = this;
            self.corrective = corrective;
        }
    }
});
