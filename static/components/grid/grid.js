(function () {

    var grid = {
        props: ['get_url', 'delete_row_url'],
        emits: ['analyze-cell', 'comp-globals', "set-thresh", "set-corrective"],
        data: null,
        methods: {}
    };

    grid.data = function () {
        var data = {
            get_url: this.get_url,
            delete_row_url: this.delete_row_url,
            has_previous: false,
            has_more: false,
            search_placeholder: '',
            search_text: '',
            page: 1,
            rows: [],
            header: {},
            editing: false,
            loading: false,
            sortable: false,
            iscorrective: false,
            min_thresh: 0.0,
            red_class: "",
            pruneable: [],
            is_selected: NaN,
            field_selected: NaN,
            is_reset: false,
            is_corrective: false,
            error:{
                status: 300,
                message: "",
                active: false
            }
        };
        grid.methods.load.call(data);
        return data;
    };

    grid.enumerate = function (a) {
        // Adds an _idx attribute to each element of array a.
        let k = 0;
        a.map(function (e) { e._idx = k++; });
    };

    grid.methods.doEmit = function (row_idx, cell_idx) {
        let self = this;
        let first_cell = self.rows[row_idx]["cells"][cell_idx];
        self.is_selected = row_idx;
        self.field_selected = cell_idx;
        console.log(self.is_selected);
        let field = self.header["cells"][cell_idx]["text"];
        let row = first_cell["emit"]["row"];
        console.log("doEmit", row, field);
        this.$emit('analyze-cell', row, field);
    }

    grid.methods.superSearch = function (row_idx, cell_idx) {
        let self = this;
        let first_cell = self.rows[row_idx]["cells"][1]['text'];
        self.is_selected = row_idx;
        self.field_selected = cell_idx;
        self.search_text = first_cell;
        self.load();
        console.log(first_cell);
    }

    grid.methods.do_search = function () {
        let self = this;
        self.page = 1; // Restart from page 1 for every search.
        self.load();
    };

    grid.methods.clear_search = function () {
        let self = this;
        self.clear_sort();
        self.search_text = "";
        self.page = 1;
        self.load();
    };

    grid.methods.search_enter = function (e) {
        let self = this;
        if (e.keyCode === 13) {
            self.do_search();
            e.target.blur();
        }
    }

    grid.methods.clear_sort = function () {
        let self = this;
        let header = self.header;
        if (!self.sortable) { return; }
        for (let cell of header.cells) {
            cell.sort = 0;
        }
        self.page = 1;
        console.log(header.cells);
    }

    grid.methods.do_sort = function (cell_idx) {
        let self = this;
        let header = self.header;
        if (!self.sortable) { return; }
        for (let cell of header.cells) {
            if ((typeof (cell_idx) === "string" && cell.text.toLowerCase() === cell_idx.toLowerCase()) ||
                (typeof (cell_idx) === "number" && cell._idx === cell_idx)) {
                // Toggles sort of selected cell.
                if (cell.sortable) {
                    cell.sort = cell.sort + 1;
                    if (cell.sort === 2) {
                        cell.sort = -1;
                    }
                }
            } else {
                // Other cells are not sorted.
                cell.sort = 0;
            }
        }
        self.page = 1;
        self.load();
    }

    grid.methods.pruneDF = function () {
        let self = this;
        console.log("Pruning with val: ", self.red_class, self.min_thresh);
        this.$emit("set-thresh", self.red_class, self.min_thresh);
        self.clear_sort();
        self.search_text = "";
        self.page = 1;
        self.load({ prune_class: self.red_class, min_thresh: self.min_thresh });
    }

    grid.methods.resetDF = function () {
        let self = this;
        self.red_class = "";
        self.min_thresh = 0.0;
        this.$emit("set-thresh", "", 0.0);
        this.$emit("set-corrective", false);
        self.clear_sort();
        self.search_text = "";
        self.page = 1;
        self.load({ is_reset: true });
    }

    grid.methods.setCorrective = function () {
        let self = this;
        this.$emit("set-corrective", true);
        self.clear_sort();
        self.search_text = "";
        self.page = 1;
        self.load({ is_corrective: true });
    }

    grid.methods.load = function (optional_fields = {}) {
        // In use, self will correspond to the data of the table,
        // as this is called via grid.methods.load
        let self = this;
        let sort_order = null;

        self.loading = true;
        if (Object.keys(self.header).length !== 0) {
            sort_order = JSON.stringify(self.header.cells.map(c => c.sort));
        }

        let vars = {
            page: self.page,
            q: self.search_text,
            sort_order: sort_order,
        };

        if (Object.keys(optional_fields).length > 0) {
            for (let name of Object.keys(optional_fields)) {
                let val = optional_fields[name];
                vars[name] = val;
            }
        }

        axios.post(self.get_url, vars)
            .then(function (res) {
                self.page = res.data.page;
                self.has_more = res.data.has_more;
                self.has_previous = self.page > 1;
                self.search_placeholder = res.data.search_placeholder;
                self.sortable = res.data.sortable;
                let rows = res.data.rows;
                let headers = res.data.header;
                grid.enumerate(rows);

                for (let r of rows) {
                    grid.enumerate(r.cells);
                    for (let c of r.cells) {
                        if (!c.sort) {
                            // Note that on purpose, we can have sorted fields
                            // that are not sortable via a UI click.
                            c.sort = 0;
                        }
                    }
                }
                grid.enumerate(headers.cells);
                for (let c of headers.cells) {
                    if (!c.sort) {
                        // Note that on purpose, we can have sorted fields
                        // that are not sortable via a UI click.
                        c.sort = 0;
                    }
                }
                self.rows = rows;
                self.header = headers;
                self.pruneable = headers["cells"].filter((word) => {
                    return word["text"].includes("Δ_");
                });

                self.loading = false;
                self.red_class = "";
                self.min_thresh = 0.0;
                self.is_selected = NaN;
                self.field_selected = NaN;
            }).catch(function (res) {
                let resp = res.response;
                if (resp.status === 410) {
                    self.error.status = 410;
                    self.error.message = "The dataset could not be loaded.";
                    self.error.active = true;
                }
            });
    };

    grid.methods.incpage = function (inc) {
        let self = this;
        i = parseInt(inc);
        if ((i > 0 && self.has_more) || (i < 0 && self.has_previous)) {
            self.page += i;
            if (self.page < 1) self.page = 1;
            self.load();
        }
    };

    grid.methods.delCol = function (column) {
        let self = this;
        if (column != "") {
            self.editing = false;
            self.loading = true;
            axios.post(self.delete_row_url,
                { row: column })
                .then(function (res) {
                    self.load();
                    self.loading = false;
                }).catch(function (res) {
                    console.log(res);
                    let resp = res.response;
                    if (resp.status === 400) {
                        self.error.status = 400;
                        self.error.message = "Unable to delete the selected column.";
                        self.error.active = true;
                    }
                });
        }

    };
    grid.methods.isDisabled = function () {
        let self = this;
        if (self.min_thresh < 0.01 || self.red_class === "") {
            return true;
        }
        return false;
    };

    grid.methods.do_reset = function () {
        let self = this;
        self.editing = false;
        self.load({ rowreset: true });

    };

    Q.register_vue_component('grid', 'components/grid/grid.html', function (template) {
        grid.template = template.data;
        return grid;
    });
})();
