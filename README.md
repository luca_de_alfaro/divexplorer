# DivExplorer Py4web Application:

## Installation

*Note that this program requires [py4web](https://github.com/web2py/py4web). While it can be installed over pip, I recommend cloning and installing from github for the simplest setup.*

**When installing `py4web`, make sure to `pip install pyjwt==1.7.1` not `>2.0` due to some framework bugs.**

0. Clone this repository to within the `apps` folder created in your py4web installation.

1. Create the virtual environment for DivExplorer using the command 
    `python3 -m venv env`. *You may also use Conda if you choose.*

2. Activate the DivExplorer virtual environment. 
    
    a. For POSIX platforms (macOS and Linux) run:
        `source ./env/bin/activate`

    b. For Windows:
        `.\env\Scripts\activate.bat`

3. Install the dependencies for DivExplorer to run using the command 
    `pip install -r ./requirements.txt`
    - **Note that within the requirements.txt file there contains two packages not on pip, DivExplorer, and NQGCS, and must be downloaded seperately.**

4. Obtain all necessary private files:
    - The `gcs_keys.json` file will contain the keys necessary for the service account used in GCS. This file should like in `private/gcs_keys.json`
    - `settings_private.py` will contain all of the necessary private variables necessary for operation. This includes:
        - `DB_USER/DB_PASSWORD/DB_NAME/DB_CONNECTION`: The information necessary for Google App Engine (Only required if deploying)
        - `MY_OAUTH2GOOGLE_CLIENT_ID`/`MY_OAUTH2GOOGLE_CLIENT_SECRET`: The OAUTH information necessary for allowing sign in with Google. **(REQUIRED)**
        - `MY_SESSION_SECRET_KEY`: The secret key for the user session, can be set to any random key during development (Must be changed in production) **(REQUIRED)**
        - `MY_BUCKET_NAME`: The name of the GCS Bucket necessary for file upload/download. **(REQUIRED)**


5. Note that for some features to work, (such as displaying the global Shapely values in a website instead of saving a file), the `FP_Divexplorer` class has been subclassed in the `classes.py` file.

## Running the program

1. Start py4web as normal, and navigate to `127.0.0.1:8000/divexplorer`. You can change the host and port if your setup is different, but note that it seems OAUTH only allows this specific path for now.

2. Start by selecting **Create Dataset** on the homepage, which will ask you to sign in, where you can **Login with Google**. After logging in you will be able to upload your dataset through giving it a title and the path to the CSV file that stores it. **NOTE THAT YOU MUST UPLOAD A DATASET THAT HAS ALREADY BEEN DISCRETIZED.**

    **For COMPAS this would be the final verison of `X_discretized` in part 10 of `F1_COMPAS-MultipleMetrics_Support_0.05.ipynb` after specifying the true and predicted classes.**

3. After you have finished uploading your dataset, you will be redirected to a page that will allow you to view the dataset through the **Display Dataset** button, and view what you uploaded. In addition, you are able to set all of the necessary fields to run the divexplorer algorithm on it.

4. Once you have chosen all the necessary fields, hit **Set Classes** which will store the information, and allow you to compute your DivExplorer dataset when you then hit **Compute DivExplorer**.

5. After this you will see a new Dataframe appear in the list below, with a description about the fields set for it. Select view to go to the next page.

6. This will show the computed Dataframe in a scrollable grid, and has some implemented options below, such as *Compute and Display Global FPR Values*, and *Compute and Display Global FPR Values*.

7. Individual records in the DivExplorer dataframe can be explorered more in depth through the **Analyze** button on the end of it's row. This page will display graphs of the individual fields that contributed the most to the record when computing the Shapely Values.