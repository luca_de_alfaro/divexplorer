import json
import matplotlib
import matplotlib.pyplot as plt
from py4web import request, URL

import re

from .util import *
from divexplorer.FP_Divergence import FP_Divergence, abbreviateDict
from .components.grid import Grid

matplotlib.use("Agg")

class mFP_Divergence(FP_Divergence):
    '''
    This subclass allows for the plotShapleyValue function
    to be able to accept a Bytesio object and save the image
    to that rather than to a local file. This allows the server
    to be able to create the image and then display it on the webpage.
    '''

    def plotShapleyValue(
        self,
        itemset=None,
        shapley_values=None,
        sortedF=True,
        metric="",
        nameFig=None,
        saveFig=False,
        height=0.5,
        linewidth=0.8,
        sizeFig=(4, 3),
        labelsize=10,
        titlesize=15,
        title=None,
        abbreviations={},
        xlabeltitle="Global Contribution",
        order=None
    ):
        if shapley_values is None and itemset is None:
            # todo
            print("Error")
            return -1
        if shapley_values is None and itemset:
            shapley_values = self.computeShapleyValue(itemset)
        if abbreviations:
            shapley_values = abbreviateDict(shapley_values, abbreviations)
        sh_plt = {str(",".join(list(k))): v for k, v in shapley_values.items()}
        metric = f"Δ_{{{self.metric_name}}}" if metric is None else metric
        if sortedF:
            if order:
                index_map = {v: i for i, v in enumerate(order)}
                sh_plt = {k: v for k, v in sorted(
                    sh_plt.items(), key=lambda pair: index_map[pair[0]])}
            else:
                sh_plt = {k: v for k, v in sorted(
                    sh_plt.items(), key=lambda item: item[1])}

        plt.barh(
            range(len(sh_plt)),
            sh_plt.values(),
            height=height,
            align="center",
            color="#7CBACB",
            linewidth=linewidth,
            edgecolor="#0C4A5B",
        )
        plt.yticks(range(len(sh_plt)), list(sh_plt.keys()), fontsize=labelsize)
        plt.xticks(fontsize=labelsize)
        plt.xlabel(
            xlabeltitle, size=labelsize
        )  # - Divergence contribution
        # title="Divergence" if title is None else title
        title = "" if title is None else title
        title = f"{title} ${metric}$" if metric != "" else title  # Divergence
        plt.title(title, fontsize=titlesize)
        plt.rcParams["figure.figsize"], plt.rcParams["figure.dpi"] = sizeFig, 100
        if saveFig:
            if isinstance(nameFig, BytesIO):                # CHANGED FEATURE OF DIVEXPLORER
                plt.savefig(nameFig, bbox_inches="tight",   # CHANGED FEATURE OF DIVEXPLORER
                            pad_inches=0.05, format="png")  # CHANGED FEATURE OF DIVEXPLORER
                plt.clf()                                   # CHANGED FEATURE OF DIVEXPLORER
                return sh_plt                                    # CHANGED FEATURE OF DIVEXPLORER
            nameFig = "./shap.pdf" if nameFig is None else nameFig
            plt.savefig(f"{nameFig}.pdf", bbox_inches="tight", pad_inches=0.05)
        plt.show()
        return sh_plt


class myGrid(Grid):
    def __init__(self, pagin_amt=5, *args, **kwargs):
        Grid.__init__(self, *args, **kwargs)
        self.pagin_amt = pagin_amt

    def formatFrozenset(self, title):
        if isinstance(title, frozenset):
            strrep = ', '.join(title)
            title = f"({strrep})"  # Remove frozenset from title
        return title

    def buildHeader(self, sort_order, df, column, isComputed):
        header = dict(is_header=True)
        cells = []
        for i in column:
            name = df.columns[i]
            if name == "viz":
                continue
            cell = dict(text=str(name))
            cell["sortable"] = isComputed
            cells.append(cell)

        header["cells"] = cells

        if (sort_order is not None):
            for hc, so in zip(header["cells"], json.loads(sort_order)):
                hc["sort"] = so
                if(so == 1):
                    df = df.sort_values(by=hc["text"], ascending=False)
                if(so == -1):
                    df = df.sort_values(by=hc["text"], ascending=True)
        return header, df

    def buildRange(self, start, end, df, columns, isComputed):
        rows = []
        divergence_check = ['d_fpr', 'd_error', 'd_fnr', 'd_accuracy']
        for i in range(start, end):
            row = dict(cells=[])
            z = zip(df.iloc[i, columns].values, df.iloc[i, columns].index)
            for value, column in z:
                if column == "viz":
                    continue
                text = str(self.formatFrozenset(value))
                if isComputed and (NAME_MAP[column] in divergence_check) and df.iloc[i]["Itemset"] != frozenset():
                    emit = (dict(row=df.iloc[i, columns].name))
                else:
                    emit = False
                r = dict(text=text, emit=emit)
                row["cells"].append(r)
            rows.append(row)
        return rows

    def _msearch(self, x, search_query):
        search_query = re.sub('[\(\)]', '', search_query)
        l = list(map(str.strip, search_query.split(",")))
        return frozenset(l).issubset(x)

    def doSearch(self, search_query, df):

        return df[df["Itemset"].apply(
            lambda x: self._msearch(x, search_query))]

    def doRemoveRow(self, row_to_delete, important_fields, df):
        try:
            col = df.columns.get_loc(row_to_delete)
        except:
            col = -1
        li = important_fields.column_list
        if col != -1 and col in li:
            li.remove(col)
            important_fields.update_record()

    def delRow(self, pk=None):
        row_to_delete = request.json.get("row")

        if not row_to_delete:
            raise HTTP(400)

        dataframe, important_fields, df = getDataframe(pk)
        isComputed = not dataframe.main_frame

        if row_to_delete != "":
            self.doRemoveRow(NAME_MAP[row_to_delete], important_fields, df)

        return "ok"

    def api(self, pk=None):
        # variables obtained with request.query
        q = request.json.get("q", "")  # Query string
        sort_order = request.json.get("sort_order") or None
        page = int(request.json.get("page")) or 1
        rowreset = request.json.get("rowreset", False)
        min_thresh = float(request.json.get("min_thresh", -1)) or None
        prune_class = request.json.get("prune_class") or None
        is_reset = request.json.get("is_reset", False)
        is_corrective = request.json.get("is_corrective", False)

        dataframe, important_fields, df = getDataframe(pk)
        isComputed = not dataframe.main_frame
        itmset = None

        if min_thresh > 0 and prune_class:
            prune_class = NAME_MAP[prune_class]
            if (important_fields.min_thresh != min_thresh) or (important_fields.prune_field != prune_class):
                df[VIZ_COL_NAME] = True
                fp_divergence_fpr = mFP_Divergence(df, prune_class)
                indexes_red_summary = fp_divergence_fpr.getIndexesRedundancySummarization(
                    th_redundancy=min_thresh)
                df.loc[(~df.index.isin(indexes_red_summary)),
                       VIZ_COL_NAME] = False
                important_fields.min_thresh = min_thresh
                important_fields.prune_field = prune_class
                important_fields.update_record()
                df["itemsets"] = df.apply(lambda x: list(
                    x["itemsets"]), axis=1)
                uploadDataframe(df, dataframe.upload_name, prehashed=True)
                df["itemsets"] = df.apply(lambda x: frozenset(
                    x["itemsets"]), axis=1)

        if is_corrective:
            if important_fields.is_corrective != is_corrective:
                df[VIZ_COL_NAME] = True
                fp_divergence_fpr = mFP_Divergence(df, "d_fpr")
                itmset = fp_divergence_fpr.getIndexesCorrectiveItemsets()
                df.loc[(~df.index.isin(itmset)),
                       VIZ_COL_NAME] = False
                important_fields.is_corrective = is_corrective
                important_fields.update_record()
                df["itemsets"] = df.apply(lambda x: list(
                    x["itemsets"]), axis=1)
                uploadDataframe(df, dataframe.upload_name, prehashed=True)
                df["itemsets"] = df.apply(lambda x: frozenset(
                    x["itemsets"]), axis=1)
        if is_reset:
            df[VIZ_COL_NAME] = True
            important_fields.min_thresh = 0.0
            important_fields.is_corrective = False
            important_fields.prune_field = ""
            important_fields.update_record()
            df["itemsets"] = df.apply(lambda x: list(
                x["itemsets"]), axis=1)
            uploadDataframe(df, dataframe.upload_name, prehashed=True)
            df["itemsets"] = df.apply(lambda x: frozenset(
                x["itemsets"]), axis=1)

        if isComputed:
            df = printable(df)
            df = df.loc[itmset] if itmset else df.loc[df[VIZ_COL_NAME]]

        df = df.round(6)
        if rowreset == True:
            if len(important_fields.column_list) != len(df.columns):
                important_fields.column_list = [
                    i for i in range(len(df.columns))]
                important_fields.update_record()

        if q != "":
            df = self.doSearch(q, df)
        start = (page - 1) * self.pagin_amt
        start = start if start <= len(
            df.index) else len(df.index) - self.pagin_amt
        end = (
            start + self.pagin_amt
            if start <= len(df.index) - self.pagin_amt
            else len(df.index)
        )
        has_more = end <= (len(df.index) - self.pagin_amt)
        header, df = self.buildHeader(
            sort_order, df, important_fields.column_list, isComputed)
        rows = self.buildRange(
            start, end, df, important_fields.column_list, isComputed)
        return dict(
            page=int(page),
            search_placeholder=self.search_placeholder,
            has_more=has_more,
            header=header,
            rows=rows,
            sortable=isComputed,
        )
