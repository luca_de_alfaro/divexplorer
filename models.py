"""
This file defines the database models
"""

from .common import db, Field, T, auth
from pydal.validators import *
from .settings import UPLOAD_FOLDER
from py4web import URL
import datetime


def get_user_email():
    return auth.current_user.get("email") if auth.current_user else None


def get_time():
    return datetime.datetime.utcnow()


def uploadfields():
    fields = \
        [
            Field("name", requires=IS_NOT_EMPTY(error_message=T(
                "Dataset Title Must Not Be Empty")), label=T("Dataset Title")),
            Field("upload_file", "upload", requires=[IS_NOT_EMPTY(error_message=T("A Dataset Must Be Uploaded")), IS_FILE(
                extension="csv", error_message=T("File Must be of Type CSV"))], label=T("Dataset Upload (CSV)"), uploadfolder=None)
        ]
    return fields


db.define_table(
    "datasets",
    Field("name", requires=IS_NOT_EMPTY(error_message=T(
        "Dataset Title Must Not Be Empty")), label=T("Dataset Title")),
    Field("author", default=get_user_email, readable=False, writable=False),
    Field("upload_name", "text"),
    Field("date_created", "datetime", default=get_time,
          readable=False, writable=False),
    Field("date_updated", "datetime", update=get_time,
          readable=False, writable=False),
    format="%(name)s",
)

db.define_table(
    "dataframes",
    Field("dataset", "reference datasets", readable=False, writable=False),
    Field("main_frame", "boolean", default=False),
    Field("computation_method", "text", default=""),
    Field("upload_name", "text"),
    Field("date_created", "datetime", default=get_time,
          readable=False, writable=False),
    Field("date_updated", "datetime", update=get_time,
          readable=False, writable=False),
)

db.define_table(
    "important_fields",
    Field("dataframe", "reference dataframes", readable=False, writable=False),
    Field("column_list", "list:integer"),
    Field("selected_fields", "list:integer"),
    Field("min_thresh", "float", default=0.0),
    Field("prune_field", "string", default=""),
    Field("is_corrective", "boolean", default=False),
    Field("true_class", "string", default="", required=True),
    Field("pred_class", "string", default="", required=False),
    Field("min_supp", "float", default=0.1, required=True),
    Field("neg_class", "string", default="0", required=True),
    Field("pos_class", "string", default="1", required=True),
    Field("date_created", "datetime", default=get_time,
          readable=False, writable=False),
    Field("date_updated", "datetime", update=get_time,
          readable=False, writable=False),
)

db.datasets.ondelete = "CASCADE"
db.commit()
