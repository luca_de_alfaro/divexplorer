from matplotlib.ticker import FuncFormatter
from nqgcs import NQGCS
from io import BytesIO
import os
from .settings import BUCKET_NAME, UPLOAD_FOLDER
from .controllers import db
import pandas as pd
import pickle
import hashlib
from collections import defaultdict
from py4web import HTTP
VIZ_COL_NAME = "viz"

nqgcs = NQGCS()


class keydefaultdict(defaultdict):
    def __missing__(self, key):
        ret = self[key] = key
        return ret


NAME_MAP = keydefaultdict(None, dict(Support="support",
                                     Itemset="itemsets",
                                     Δ_fpr="d_fpr",
                                     t_fp="t_value_fp",
                                     Δ_fnr="d_fnr",
                                     t_fn="t_value_fn",
                                     Δ_error="d_error",
                                     t_error="t_value_tp_tn",
                                     Δ_acc="d_accuracy",
                                     FPR="fpr",
                                     FNR="fnr",
                                     Acc="accuracy",
                                     Support_count="support_count",
                                     Error="error",
                                     t_fp_fn="t_value_fp_fn",
                                     Tn="tn",
                                     Fp="fp",
                                     Fn="fn",
                                     Tp="tp",
                                     Length="length",
                                     viz="viz"))


def uploadFile(filename, f, type="application/octet-stream"):
    nqgcs.upload(BUCKET_NAME, filename, f, type=type)


def deleteFile(filename):
    try:
        nqgcs.delete(BUCKET_NAME, filename)
    except:
        return -1
    return 0


def hashName(file):
    return hashlib.md5(file.encode()).hexdigest()


def convertDataframe(upload_file):
    # Convert dataframe from CSV user input
    csv_name = upload_file.filename
    file = upload_file.file
    hashed = hashName(os.path.splitext(csv_name)[0]) + ".ftr"
    df = pd.read_csv(file)
    f = BytesIO()
    df.to_feather(f)
    columns = len(df.columns)
    columns = [i for i in range(columns)]
    f.seek(0)
    uploadFile(hashed, f)
    return hashed, columns


def uploadDataframe(df, name, prehashed=False):
    # Upload DivExplorer Computed Dataset
    f = BytesIO()
    df.to_feather(f)
    hashed = hashName(os.path.splitext(
        name)[0]) + "-computed.ftr" if not prehashed else name
    columns = len(df.columns)
    columns = [i for i in range(columns)]
    f.seek(0)
    uploadFile(hashed, f)
    return hashed, columns


def readDataframe(filename):
    try:
        file = nqgcs.read(BUCKET_NAME, filename)
        f = BytesIO(file)
        f.seek(0)
        df = pd.read_feather(f)
        return df
    except:
        return None


def getDataframe(pk):
    dataframe = db.dataframes[pk]
    important_fields = db.dataframes(
        dataframe.id).important_fields.select().first()

    if dataframe is None:
        raise HTTP(410)

    df = readDataframe(dataframe.upload_name)

    if df is None:
        print("Dataframe doesn't exist")
        raise HTTP(410)

    if 'itemsets' in df and not dataframe.main_frame:
        df["itemsets"] = df.apply(lambda x: frozenset(
            x["itemsets"]), axis=1)  # Reconvert string to frozenset
    return dataframe, important_fields, df


def printable(df_print):
    # Item name in the paper
    i_name = "α"  # or i
    # Pattern or itemset name in the paper
    p_name = "I"
    # Name for diverge in the paper
    div_name = "Δ"

    df_print = df_print.copy()
    if "support" in df_print.columns:
        df_print["support"] = df_print["support"].round(2)
    t_v = [c for c in df_print.columns if "t_value_" in c]
    if t_v:
        df_print[t_v] = df_print[t_v].round(1)
    df_print = df_print.round(3)
    df_print.rename(columns={"itemsets": "itemset"}, inplace=True)
    df_print.columns = df_print.columns.str.replace("d_", f"{div_name}_")
    df_print.columns = df_print.columns.str.replace("accuracy", "acc")
    df_print.columns = df_print.columns.str.capitalize()
    df_print.columns = df_print.columns.str.replace("T_value", "t")
    df_print.rename(columns={"t_tp_tn": "t_error"}, inplace=True)
    df_print.columns = df_print.columns.str.replace("Fnr", f"FNR")
    df_print.columns = df_print.columns.str.replace("Fpr", f"FPR")
    df_print.columns = df_print.columns.str.replace("Viz", f"viz")
    return df_print
