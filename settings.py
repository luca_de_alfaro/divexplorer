"""
This is an optional file that defined app level settings such as:
- database settings
- session settings
- i18n settings
This file is provided as an example:
"""
import os
import json

try:
    from .settings_private import *
except:
    pass

# db settings
APP_FOLDER = os.path.dirname(__file__)
APP_NAME = os.path.split(APP_FOLDER)[-1]

# DB_FOLDER:    Sets the place where migration files will be created
#               and is the store location for SQLite databases
DB_FOLDER = os.path.join(APP_FOLDER, "databases")
DB_POOL_SIZE = 1
DB_MIGRATE = True
DB_FAKE_MIGRATE = False  # maybe?

# This is the URL of MySQL as accessed from Google Appengine
if os.environ.get("GAE_ENV"):
    GAE_DB_URI = "google:MySQLdb://{DB_USER}:{DB_PASSWORD}@/{DB_NAME}?unix_socket=/cloudsql/{DB_CONNECTION}".format(
        DB_USER=DB_USER,
        DB_NAME=DB_NAME,
        DB_PASSWORD=DB_PASSWORD,
        DB_CONNECTION=DB_CONNECTION,
    )

TESTING_DB_URI = "sqlite://storage.db"

# location where to store uploaded files:
UPLOAD_FOLDER = None

# send email on regstration
VERIFY_EMAIL = False

# account requires to be approved ?
REQUIRES_APPROVAL = False

# email settings
SMTP_SSL = False
SMTP_SERVER = None
SMTP_SENDER = "you@example.com"
SMTP_LOGIN = "username:password"
SMTP_TLS = False

# session settings
SESSION_TYPE = "cookies"
SESSION_SECRET_KEY = MY_SESSION_SECRET_KEY  # replace this with a uuid
MEMCACHE_CLIENTS = ["127.0.0.1:11211"]
REDIS_SERVER = "localhost:6379"

# logger settings
LOGGERS = [
    "warning:stdout"
]  # syntax "severity:filename" filename can be stderr or stdout

# single sign on Google (will be used if provided)
OAUTH2GOOGLE_CLIENT_ID = MY_OAUTH2GOOGLE_CLIENT_ID
OAUTH2GOOGLE_CLIENT_SECRET = MY_OAUTH2GOOGLE_CLIENT_SECRET

# enable PAM
USE_PAM = False

# enable LDAP
USE_LDAP = False
LDAP_SETTINGS = {
    "mode": "ad",
    "server": "my.domain.controller",
    "base_dn": "ou=Users,dc=domain,dc=com",
}

# i18n settings
T_FOLDER = os.path.join(APP_FOLDER, "translations")

# Celery settings
USE_CELERY = False
CELERY_BROKER = "redis://localhost:6379/0"

GCS_KEY_PATH = os.path.join(APP_FOLDER, "private/gcs_keys.json")
with open(GCS_KEY_PATH) as gcs_key_f:
    GCS_KEYS = json.load(gcs_key_f)

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = GCS_KEY_PATH


BUCKET_NAME = MY_BUCKET_NAME
