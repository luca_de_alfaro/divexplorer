"""
This file defines actions, i.e. functions the URLs are mapped into
The @action(path) decorator exposed the function at URL:

    http://127.0.0.1:8000/{app_name}/{path}

If app_name == '_default' then simply

    http://127.0.0.1:8000/{path}

If path == 'index' it can be omitted:

    http://127.0.0.1:8000/

The path follows the bottlepy syntax.

@action.uses('generic.html')  indicates that the action uses the generic.html template
@action.uses(session)         indicates that the action uses the session
@action.uses(db)              indicates that the action uses the db
@action.uses(T)               indicates that the action uses the i18n & pluralization
@action.uses(auth.user)       indicates that the action requires a logged in user
@action.uses(auth)            indicates that the action requires the auth object

session, db, T, auth, and tempates are examples of Fixtures.
Warning: Fixtures MUST be declared with @action.uses({fixtures}) else your app will result in undefined behavior
"""

from py4web import action, request, abort, redirect, URL, HTTP
from yatl.helpers import A
from py4web.utils.form import Form, FormStyleBulma

from .common import db, session, T, auth, authenticated, unauthenticated, flash, signed_url
from .settings import UPLOAD_FOLDER
from .models import get_user_email, uploadfields
from .classes import *

import pandas as pd
import numpy as np
import feather
import json
import base64
from datetime import datetime

from divexplorer.FP_DivergenceExplorer import FP_DivergenceExplorer
from divexplorer.shapley_value_FPx import normalizeMax

vue_grid = myGrid(
    path="grid_api",
    session=session,
    db=db,
    search_placeholder="Search for specific records here",
)


@authenticated("admin", "admin.html")
def admin():
    email = get_user_email()
    if email != "agavgavi@ucsc.edu":
        redirect("index")
    datasets = db(db.datasets).select(
        orderby=~db.datasets.date_created
    )
    users = db(db.auth_user).select()
    return dict(hello="hello", datasets=datasets, users=users)


@unauthenticated("index", "index.html")
def index():
    user = auth.get_user()
    message = T("Hello {first_name}".format(**user) if user else "Hello")
    return dict(message=message)


# @unauthenticated("about", "about.html")
# def about():
#     return dict()


@authenticated("dataset/new", "create_dataset.html")
def datasetCreate():
    fields = uploadfields()
    form = Form(fields, csrf_session=session,
                formstyle=FormStyleBulma, dbio=False)

    if form.accepted:
        # We always want POST requests to be redirected as GETs
        hashed, columns = convertDataframe(form.vars["upload_file"])
        dataset_id = db.datasets.insert(
            name=form.vars["name"], upload_name=hashed)
        dataframe_id = db.dataframes.insert(
            dataset=dataset_id, main_frame=True, upload_name=hashed
        )
        important_fields = db.important_fields.insert(
            dataframe=dataframe_id, column_list=columns, selected_fields=[]
        )

        redirect(URL("dataset", dataset_id))
    return dict(form=form)


@action("dataset/<pk:int>", method=["GET"])
@action.uses(db, signed_url, T, flash, auth.user, vue_grid, "grid_detail.html")
def datasetDetail(pk=None):
    if pk is None:
        redirect(URL("index"))
    dataset = db.datasets[pk]
    if dataset is None or dataset.author != get_user_email():
        redirect(URL("index"))
    d_pk = db((db.dataframes.dataset == dataset) &
              (db.dataframes.main_frame == True)).select().first()

    return dict(
        name=dataset.name,
        grid_urls=vue_grid.url(d_pk.id),
        dataframe_url=URL("dataframe/get", pk, signer=signed_url),
        get_url=URL("dataset/get_settings", pk, signer=signed_url),
        set_url=URL("dataset/set_settings", pk),
        compute_url=URL("dataset/computeDiv", pk, signer=signed_url),
        dataframe_view_url=URL("dataframe"),
        delete_url=URL("dataframe/delete"),
    )





@action("dataframe/<d_pk:int>", method=["GET"])
@action.uses(db, session, T, flash, auth.user, vue_grid, "dataframe_detail.html")
def dataframeDetail(d_pk=None):
    if d_pk is None:
        redirect(URL("index"))
    dataframe = db.dataframes[d_pk]

    if dataframe is None or dataframe.dataset.author != get_user_email():
        redirect(URL("index"))

    imp_f = db(db.important_fields.id ==
               dataframe.important_fields.id).select().first()

    dataset = db(db.dataframes.dataset == dataframe.dataset.id).select().first()
    _, _, df = getDataframe(dataset.id)
    
    imp_f.selected_fields = list(map(lambda x: df.columns.values[x], imp_f["selected_fields"]))

    return dict(imp_f=imp_f, name=dataframe.dataset.name,
                grid_urls=vue_grid.url(d_pk),
                computeGlobal_url=URL(
                    'dataframe/computeGlobal/', imp_f.dataframe.id, signer=signed_url),
                displayShapely_url=URL(
                    'dataframe/displayShapely/', d_pk, signer=signed_url))


@authenticated("my_datasets", "user_datasets.html")
def userDatasets():

    author = get_user_email()
    if author == None:
        redirect(URL("index"))

    datasets = db(db.datasets.author == author).select(
        orderby=~db.datasets.date_created
    )
    return dict(datasets=datasets)


@authenticated("dataset/delete/<pk:int>")
def datasetDelete(pk=None):
    dataset = db.datasets[pk]

    if dataset is None or dataset.author != get_user_email():
        redirect(URL("index"))

    dataframes = db(db.dataframes.dataset == pk).select()
    for dataframe in dataframes: 
        if deleteFile(dataframe.upload_name) == -1:
            print("unable to delete dataframe")
    del db.datasets[pk]
    redirect(URL("my_datasets"))


@authenticated("dataframe/delete", method=["POST"])
def dataframeDelete(pk=None):
    try:
        pk = int(request.json.get('id'))
    except:
        raise HTTP(400)
    dataframe = db.dataframes[pk]

    if dataframe is None or dataframe.dataset.author != get_user_email():
        raise HTTP(410)

    if deleteFile(dataframe.upload_name) == -1:
        print("unable to delete dataframe")
    del db.dataframes[pk]
    redirect(URL("my_datasets"))


@action("dataset/get_settings/<pk:int>")
@action.uses(db, session, T, auth.user, signed_url.verify())
def getSettings(pk=None):
    if pk is None:
        raise HTTP(400)
    dataset = db.datasets[pk]
    if dataset is None or dataset.author != get_user_email():
        raise HTTP(410)
    dataframe = db(db.dataframes.dataset == pk).select().first()
    _, important_fields, df = getDataframe(dataframe.id)
    return dict(data=important_fields.as_dict(), columns=df.columns.values)



@action("dataframe/get/<pk:int>")
@action.uses(db, session, T, auth.user, signed_url.verify())
def getDataframes(pk=None):
    if pk is None:
        raise HTTP(400)
    dataset = db.datasets[pk]
    if dataset is None or dataset.author != get_user_email():
        raise HTTP(410)
    dataframes = db((db.dataframes.dataset == dataset) &
                    (db.dataframes.main_frame == False)).select(
        left=db.important_fields.on(db.dataframes.id == db.important_fields.dataframe)).as_list()
    return dict(dataframes=dataframes)


@authenticated("dataset/set_settings/<pk:int>")
def setSettings(pk=None):
    if pk is None:
        raise HTTP(400)
    dataset = db.datasets[pk]
    if dataset is None or dataset.author != get_user_email():
        raise HTTP(410)

    true_class = request.json.get("true_class") or None
    pred_class = request.json.get("pred_class") or None
    min_supp = float(request.json.get("min_supp")) or 0.1
    positive_class = request.json.get("pos_class") or "1"
    negative_class = request.json.get("neg_class") or "0"
    cols = request.json.get("selected_cols") or []



    if not true_class:
        raise HTTP(400)

    if min_supp < 0 or min_supp > 1:
        raise HTTP(400)

    dataframe = db(db.dataframes.dataset == pk).select().first()
    _, important_fields, df = getDataframe(dataframe.id)

    numerical_cols = [df.columns.get_loc(c) for c in cols if c in df]

    if true_class and not true_class in df.columns:
        raise HTTP(400)
    if pred_class and not pred_class in df.columns:
        raise HTTP(400)

    if true_class and not true_class in cols:
        raise HTTP(400)

    if pred_class and not pred_class in cols:
        raise HTTP(400)
    
    if len(set(cols).difference(set([true_class, pred_class]))) == 0:
        raise HTTP(400)

    if true_class and important_fields.true_class != true_class:
        important_fields.true_class = true_class

    if pred_class and important_fields.pred_class != pred_class:
        important_fields.pred_class = pred_class

    if min_supp != 0 and important_fields.min_supp != min_supp:
        important_fields.min_supp = min_supp

    if important_fields.pos_class != positive_class:
        important_fields.pos_class = positive_class

    if important_fields.neg_class != negative_class:
        important_fields.neg_class = negative_class

    if important_fields.column_list != numerical_cols:
        important_fields.column_list = numerical_cols

    important_fields.update_record()
    return "ok"


@action("dataset/computeDiv/<pk:int>", method=["POST"])
@action.uses(db, session, T, auth.user, signed_url.verify())
def computeDiv(pk=None):
    if pk is None:
        raise HTTP(400)
    dataset = db.datasets[pk]
    if dataset is None or dataset.author != get_user_email():
        raise HTTP(410)

    dataframe = db(db.dataframes.dataset == pk).select().first()
    dataframe, important_fields, df = getDataframe(dataframe.id)
    min_supp = float(important_fields.min_supp)
    # If the negative/positive classes are numbers we convert them, otherwise leave them as strings
    try:
        class_map = {"N": int(important_fields.neg_class),
                     "P": int(important_fields.pos_class)}
    except:
        class_map = {"N": str(important_fields.neg_class),
                     "P": str(important_fields.pos_class)}
    name = str(dataset.name)
    t_class = str(important_fields.true_class)
    p_class = str(important_fields.pred_class)

    cols = important_fields.column_list
    df = df.iloc[:, cols]
    fp_driver = FP_DivergenceExplorer(
        df, t_class, p_class, class_map=class_map, dataset_name=name)
    # Returns a dataframe
    FP_fm = fp_driver.getFrequentPatternDivergence(
        min_support=min_supp, metrics=["d_fpr", "d_fnr", "d_accuracy", "d_error"], viz_col=True)

    FP_fm["itemsets"] = FP_fm.apply(lambda x: list(
        x["itemsets"]), axis=1)  # Reconvert frozenset to list

    my_order = ['support', 'itemsets', 'd_fpr', 't_value_fp', 'd_fnr',  't_value_fn', 'd_error', 't_value_tp_tn', 'd_accuracy',
                'fpr', 'fnr', 'accuracy', 'support_count', 'error', 't_value_fp_fn', 'tn', 'fp', 'fn', 'tp', 'length', "viz"]
    FP_fm = FP_fm[my_order]

    hashed, columns = uploadDataframe(
        FP_fm, dataframe.upload_name+datetime.now().strftime("%m/%d/%Y, %H:%M:%S"))

    dataframe_id = db.dataframes.insert(
        dataset=dataset,
        upload_name=hashed,
        main_frame=False,
        computation_method="divexplorer"
    )

    important_fields = db.important_fields.insert(
        dataframe=dataframe_id, column_list=columns, selected_fields=cols, min_supp=min_supp,
        true_class=t_class, pred_class=p_class, neg_class=class_map["N"], pos_class=class_map["P"]
    )
    return dict(id=dataframe_id)


@action("dataframe/computeGlobal/<pk:int>", method=["POST"])
@action.uses(db, session, T, auth.user, signed_url.verify())
def computeGlobals(pk=None):

    field = request.json.get("field", "d_fpr")
    title = field
    field = NAME_MAP[field]

    if pk is None:
        raise HTTP(400)

    dataframe = db.dataframes[pk]
    if dataframe is None or dataframe.dataset.author != get_user_email():
        raise HTTP(410)

    _, important_fields, FP_fm = getDataframe(dataframe.id)
    fp_divergence_field = mFP_Divergence(FP_fm, field)

    u_h_fp = fp_divergence_field.computeGlobalShapleyValue()

    # get global bytestream
    pic_IObytes = BytesIO()
    shapely_valuesG = normalizeMax(u_h_fp)
    order = fp_divergence_field.plotShapleyValue(
        shapley_values=shapely_valuesG, sizeFig=(1.3, 1.2), title=title, saveFig=True, nameFig=pic_IObytes, xlabeltitle="Global item contribution")
    pic_IObytes.seek(0)

    order = [k for k, v in order.items()]
    # get less global bytestream
    pic_IObytes2 = BytesIO()
    shapely_values = normalizeMax(
        fp_divergence_field.getFItemsetsDivergence()[1])
    fp_divergence_field.plotShapleyValue(
        shapley_values=shapely_values, sizeFig=(1.3, 1.2), title=title, saveFig=True, nameFig=pic_IObytes2, xlabeltitle="Individual item contribution", order=order)
    pic_IObytes2.seek(0)

    Gimage = "data:image/jpeg;base64, " + \
        base64.b64encode(pic_IObytes.read()).decode("utf-8")
    pic_IObytes.close()

    image = "data:image/jpeg;base64, " + \
        base64.b64encode(pic_IObytes2.read()).decode("utf-8")
    pic_IObytes2.close()

    return dict(imgbytes1=Gimage, imgbytes2=image)


@action("dataframe/displayShapely/<pk:int>", method=["POST"])
@action.uses(db, session, T, auth.user, signed_url.verify())
def displayShapely(pk=None):

    row = request.json.get("row", None)
    field = request.json.get("field", "d_fpr")
    title = field
    field = NAME_MAP[field]
    if field not in ['d_fpr', 'd_error', 'd_fnr', 'd_accuracy', 't_value_fp', "t_value_fn", "t_value_tp_tn", "t_value_fp_fn"]:
        print("Error: Not in accepted fields")
        raise HTTP(400)

    if pk is None or row is None:
        print("Error: PK or Row is none")
        raise HTTP(400)

    dataframe = db.dataframes[pk]
    if dataframe is None or dataframe.dataset.author != get_user_email():
        raise HTTP(410)

    _, important_fields, FP_fm = getDataframe(dataframe.id)
    th_divergence = 0.15
    itemset = FP_fm.iloc[row]['itemsets']
    strrep = ', '.join(itemset)
    strrep = f"({strrep})"

    if itemset == frozenset():
        print("Error: Itemset is blank")
        raise HTTP(400)

    fp_divergence_field = mFP_Divergence(FP_fm, field)

    pic_IObytes = BytesIO()

    fp_divergence_field.plotShapleyValue(
        itemset=itemset, sizeFig=(1.3, 1.2), title=title, saveFig=True, nameFig=pic_IObytes, xlabeltitle="Individual Contribution")
    pic_IObytes.seek(0)

    fig1 = fp_divergence_field.plotLatticeItemset(
        itemset, Th_divergence=th_divergence, sizeDot="small", getLower=True, show=False, displayItemsetLabels=True, height=400, font_size_div=15, font_size_hover_labels=15, font_size_ItemsetLabels=14)

    figdata = fig1.to_plotly_json()

    image1 = "data:image/jpeg;base64, " + \
        base64.b64encode(pic_IObytes.read()).decode("utf-8")
    pic_IObytes.close()
    return dict(imgbytes1=image1, figdata=figdata, name=strrep)
